##UModel项目源代码结构

##1.首先，浏览整个项目的目录结构

![UModelEx_Structure](http://7xlqcs.com1.z0.glb.clouddn.com/UModelEx_Structure.png)


###1.1Core目录

![UModelEx_Structure_Core](http://7xlqcs.com1.z0.glb.clouddn.com/UModelEx_Structure_Core.png)

这个目录下，是一些核心的和通用的基础代码。

###1.2Exporters

![UModelEx_Structure_Exporters](http://7xlqcs.com1.z0.glb.clouddn.com/UModelEx_Structure_Exporters.png)

这下面是一些导出资源的插件。

###1.3libs

![UModelEx_Structure_libs](http://7xlqcs.com1.z0.glb.clouddn.com/UModelEx_Structure_libs.png)

这下面是UModel用到的一系列第三方库。lzo和zlib都是开源的压缩库，PowerVR是纹理解压库。

###1.4 MeshInstance

![UModelEx_Structure_MeshInstance](http://7xlqcs.com1.z0.glb.clouddn.com/UModelEx_Structure_MeshInstance.png)

和模型网格相关的类。

![UModelEx_Structure_UI](http://7xlqcs.com1.z0.glb.clouddn.com/UModelEx_Structure_UI.png)

UI相关，对话框和文件浏览界面。

###1.5 UmodelTool

![UModelEx_Structure_UmodelTool](http://7xlqcs.com1.z0.glb.clouddn.com/UModelEx_Structure_UmodelTool.png)

UModel程序的主要界面和逻辑都在这里，分析的入口从Main.cpp开始。

###1.6 Unreal

这个目录下包含了解析Unreal资源的所有代码，有材质、模型、纹理、音频等等。

###1.7 Viewers

包含了资源查看器的源代码。有材质、模型和骨骼查看器。